<?php 
/*
function string_replace($str, $reparr){
	foreach ($reparr as $key => $value) {
		$pattern = "/\[{$key}\]/i";
		$str = preg_replace($pattern, $value, $str);
	} 
	return $str;
}
// array('filename'=array('title', 'name'), )
function get_mark($filename){
	if(($str = file_get_contents($filename)) === FALSE){
		echo "file_get_contents error";
	}

	$pattern = "/\[[a-zA-Z_][a-zA-Z0-9_]*\]/i";
	if(($ret = preg_match_all($pattern, $str, $matches)) == 0){
		echo "匹配错误或没有匹配值";
	} 
	return $matches[0];
}
*/
function _readdir($dir){
	if(!is_dir($dir)){
		echo "not dir";
	}

	if (($dh = opendir($dir)) == FALSE) {
			echo "open failed";
	}

	$files_arr = array();
	while (($file = readdir($dh)) !== false) {
		if(filetype($dir.$file) == 'file'){
			$files_arr[] = $file;
		}
	}
	closedir($dh);
	return $files_arr;
}

$files_arr = _readdir("./tpl/");

?>
<html>
	<head>
		<script src="./js/jquery-1.7.2.min.js"></script>
		<link rel="stylesheet" href="./js/highlight/styles/default.css">
		<script src="./js/highlight/highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<div> 
			<form method="post" id="inputform">

			<div class="input" style="border: 1px solid red">
			<button class="delfield">删除</button>
			选择模板
			 <select name="tplname_1" class="sel">
			 	<option>选择模板</option>
					<?php foreach ($files_arr as $key => $value) {?>
			 	<option value="<?php echo $value; ?>"><?php echo $value;?></option>
					<?php } ?>
			 </select>
			 <p  class="field"></p>
			
			</div>

			</form>

			<div style="border: 2px solid blue; padding: 10px 10px ">
				<button id="addinput">添加新元素</button>	
				<button id="prtbtn">打印表单</button>	
			</div>

			
			<pre style="border: 1px solid green" ><code id="print" class="html">
				
			</code>
			</pre>
				
		</div>
	</body>

	<script type="text/javascript">
		$("body").delegate('.sel', 'change', 
			function () { 
				//var checkText = $(this).find("option:selected").text();
				var checkValue = $(this).val();
				var sel = $(this);
				//console.log(sel);
				$.post("./handle.php", { inputtpl: checkValue},
				   function(data){
				     //alert(data.name); // John
				     console.log(data); //  2pm
				     //alert("get data"); // John

				    //get mark
				    var tplname = sel.attr("name");
				    var mark_t = tplname.split('_');
				    var mark = mark_t[1];

				    var str = "";
				    for (var i=0;i<data.length;i++) {
					   str += '['+data[i]+'_'+mark+']'+'<input type="text" name="'+data[i]+'_'+mark+'" value=""/>';
					}
				    //$(".field").append(str);
				    sel.next().html(str);
					//alert(str);

				   }, "json");
				}
		);

		$("body").delegate('.delfield', 'click', function () { 
			$(this).parent(".input").get(0).remove();
		});

		$("#addinput").click( function () { 
			//var mark= Math.floor(Math.random()*100+1);
			var mark = new Date().getTime();
			var str = '<div class="input" style="border: 1px solid red"> <button class="delfield">删除</button> 选择模板 <select name="tplname_'+mark+'"  class="sel"> <?php foreach ($files_arr as $key => $value) {?> <option><?php echo $value;?></option> <?php } ?> </select> <p  class="field"></p> </div>';
			$("#inputform").append(str);
		});
		$("#prtbtn").click( function () { 
			var temp = $('#inputform').serialize();
			 //console.log(temp); 
			 //alert(temp);
			$.post("./ptr.php", temp, function(data){
				//alert(data);
				//JSON.stringify(data);
				$("#print").html(data);
				console.log(data); 
			});
		});
	</script>
</html>